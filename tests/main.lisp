(defpackage conserve/tests/main
  (:use :cl
        :conserve
        :rove))
(in-package :conserve/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :conserve)' in your Lisp.

(deftest conserve-tests

  (testing "construction and coversion to list of a lazy sequence."
           (equal (list (list 42) nil)
                  (multiple-value-list (to-list (seq 42 nil)))))

  (testing "emptyp"
    (ok
     (equal (emptyp nil) t)
     (equal (emptyp (range)) nil)))

  (testing "range"
    (ok
     (equal (to-list (range :end 10))
            (loop for i upto 9 collect i)))

    (ok
     (equal (to-list (range :start 42 :end 64))
            (loop for i from 42 to (1- 64) collect i)))

    (ok
     (equal (to-list (range :start 42 :end 64 :step 2))
            (loop for i = 42 then (+ i 2)
               until (>= i 64) collect i)))

    (ok
     (equal (to-list (range :start 1 :end 5))
            (list 1 2 3 4)))

    (ok
     (equal (to-list (range :start 10 :end 50 :step 10))
            (list 10 20 30 40 )))

    (ok
     (equal (to-list (range :start 100 :end 500 :step 100))
            (list 100 200 300 400))))

  (testing "to-list"
    (ok
     (multiple-value-bind (list more)
         (to-list (range) :max 3)
       (and more (equal (list 0 1 2) list))))

    (ok
     (multiple-value-bind (list more)
         (to-list (mapcar* #'+ (range) (take 3 (constantly* 2))))
       (and (not more) (equal (list 2 3 4) list))))

    (ok
     (multiple-value-bind (list more) (to-list nil)
       (and (not list) (not more)))))

  (testing "take"
    (ok
     (equal (to-list (take 10 (range)))
            (loop for i from 0 to 9 collect i)))

    (ok
     (equal (to-list (take 10 (range)))
            (to-list (range :end 10)))))

  (testing "reduce*"
    (ok
     (= 45 (reduce* #'+ (range :end 10))))

    (ok
     (= 658 (reduce* #'+ (range :start 16 :end 64 :step 3) :initial-value 42)))

    (null (reduce* (lambda (x y) (declare (ignore x) (ignore y)) 'foo) nil)))

  (testing "mapcar*"
    (ok
     (equal (to-list (mapcar* #'+ (range) (list 1 2 3 4 5)))
            (list 1 3 5 7 9)))

    (ok
     (null (mapcar* #'+ (range) nil)))

    (ok
     (equal (to-list (mapcar* #'+ (range) (range)) :max 5)
            (list 0 2 4 6 8))))

  (testing "filter"
    (ok
     (equal (to-list (take 5 (filter #'evenp (range))))
            (list 0 2 4 6 8))))

  (testing "interleave"
    (ok
     (equal (to-list (interleave (range :start 1 :end 5)
                                 (range :start 10 :step 10)
                                 (range :start 100 :step 100)))
            (list 1 10 100 2 20 200 3 30 300 4 40 400))))

  (testing "zip"
    (ok
     (equal (to-list (zip (range :start 1 :end 5)
                          (range :start 10 :step 10)
                          (range :start 100 :step 100)))
            (loop for i from 1 to 4
               collect (mapcar (lambda (x) (* i x)) '(1 10 100)))))))



