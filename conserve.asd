(defsystem "conserve"
  :version "0.1.0"
  :author "Michael Cornelius"
  :license "MIT"
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description "a simple laziness library for common lisp"
  :in-order-to ((test-op (test-op "conserve/tests"))))

(defsystem "conserve/tests"
  :author "Michael Cornelius"
  :license "MIT"
  :depends-on ("conserve"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for conserve"
  :perform (test-op (op c) (symbol-call :rove :run c)))
