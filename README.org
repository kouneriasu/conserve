* Conserve  - a simple lazy collection library for Common Lisp

** Summary

Conserve is a lazy collection library inspired by Clojure and [[http://blog.thezerobit.com/2012/07/28/lazy-sequences-in-common-lisp.html][this blog post.]]

** Installation

It's easiest to load this project using [[https://www.quicklisp.org/beta/][Quicklisp.]]

After cloning, add the directory to quicklisp/local-projects. Then load into your
instance by evaluating ~(ql:quickload :conserve)~.

** Usage

Conserve provides several functions useful for manipulating and
examining lazily computed, potentially infinite sequences.

You can create a new lazy sequence with the ~seq~ macro. ~seq~ takes two
arguments: a value, which is the first element of the sequence, and a
form which will, itself, evaluate to a lazy sequence.

*** Examples

A simple counting sequence is defined as follows:

#+begin_example
CL-USER> (defun counter (&optional (n 0)) (seq n (counter (1+ n))))
COUNTER
CL-USER> (counter)
(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 |...|)
CL-USER> (head *)
0
CL-USER> (tail **)
(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 |...|)
CL-USER> (head (tail ***))
1
#+end_example

Etc.

The following example produces a lazy sequence of the prime number
(using a naive algorithm).

#+BEGIN_SRC common-lisp
  (defun primep (n)
    "Return n if n is prime, nil otherwise."
    (when (>= n 2)
      (loop
         for d from 2 to (sqrt n)
         if (zerop (rem n d)) return nil
         finally (return n))))

  ;; Create a lazy sequence of the prime numbers.
  (defun primes (&optional (n 2))
    (labels ((next (n)
               (or (primep n)
                   (next (1+ n)))))
      (let ((next (next n)))
        (seq next (primes (1+ next))))))  ; Lazy sequence constructed here.
#+END_SRC

Evaluating this code, we can then look at the 42nd-47th easily (and
lazily) enough.

#+begin_example
  CL-USER> (take 5 (drop 42 (primes)))
  (191 193 197 199 211)
#+end_example

Note that because the lazy sequence is finite and short, it is printed
as a simple list. Nevertheless, the result of ~take~ is, itself, lazy.

** Author

+ Michael Cornelius (michael@ninthorder.com)

** License

*** The MIT License (MIT)

Copyright © 2022 Michael Cornelius

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
“Software”), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


