(defpackage conserve
  (:use :cl)
  (:export #:seq
           #:emptyp
           #:head
           #:tail
           #:reduce*
           #:mapcar*
           #:constantly*
           #:drop
           #:enumerate
           #:filter
           #:interleave
           #:iterate
           #:nth*
           #:range
           #:repeatedly
           #:take
           #:zip
           #:to-list
           #:|...|))
(in-package :conserve)

(defclass lazy-sequence ()
  ((val :initarg :val)))

(defmacro seq (head tail)
  "Create a new lazy sequence.

HEAD is the first value sequence, while TAIL is, itself, a lazy sequence."
  `(make-instance 'lazy-sequence
                  :val (cons ,head (lambda () ,tail))))

(defgeneric emptyp (seq)
  (:documentation "True there are more elements in the sequence; nil otherwise."))
(defgeneric head (seq)
  (:documentation "Access the next element in the sequence. nil if sequence is empty."))
(defgeneric tail (seq)
  (:documentation "Access the \"tail\" of the sequence, itself a lazy sequence"))

(defmethod emptyp ((seq lazy-sequence))
  nil)

(defmethod emptyp ((seq list))
  (null seq))


(defmethod head ((seq lazy-sequence))
  (car (slot-value seq 'val)))

(defmethod head ((seq list))
  (car seq))

(defmethod tail ((seq lazy-sequence))
  (funcall (cdr (slot-value seq 'val))))

(defmethod tail ((seq list))
  (cdr seq))


(defun range (&key (start 0) end (step 1))
  "Produce a lazy sequence integers defined by START, END, and STEP. If end
is NIL, returned sequence is infinite."
  (unless (and end
               (or (and (< step 0)
                        (<= start end))
                   (and (> step 0)
                        (>= start end))))
    (seq start (range :start (+ start step) :end end :step step))))

(defun take (n seq)
  "Produce a lazy sequence consisting of the first N elements of lazy sequence SEQ."
  (unless (or (emptyp seq) (zerop n))
    (seq (head seq)
         (take (1- n) (tail seq)))))

(defun drop (n seq)
  "Produce a lazy sequence consisting of the sequence that remains after
discarding the first N elements of sequence SEQ."
  (if (zerop n)
      seq
      (drop (1- n) (tail seq))))

(defun reduce* (f seq
                &key initial-value
                &aux (accum (or initial-value
                                (prog1
                                    (head seq)
                                  (setq seq (tail seq))))))
  "Reduce sequence SEQ by function F.

F is a function of two arguments. The first is the result of F from the
previous iteration of the reduction. The second is the next element in
the sequence. For the first iteration, if INITIAL-VALUE is not provided,
the first element of the sequence will be used, and REDUCE* will iterate
over (TAIL SEQ).

Examples:
  (reduce* #'+ (range 10)) ;;-> 45
  (reduce* #'+ (range :start 16 :end 64 :step 3) :initial-value 42) ;;-> 658"
  (labels ((aux (seq accum)
             (if (emptyp seq) accum
                 (aux (tail seq) (funcall f accum (head seq))))))
    (aux seq accum)))

(defun mapcar* (fn seq1 &rest rest &aux (seqs (cons seq1 rest)))
  "Produce a lazy sequence consisting of the result of applying FN to
the successive heads of the sequences provided as arguments."
  (cond
    ((some #'emptyp seqs) nil)
    (t (seq (apply fn (mapcar #'head seqs))
            (apply #'mapcar* fn (mapcar #'tail seqs))))))

(defun filter (pred seq)
  "Produce a lazy sequence consisting of elements of SEQ for which PRED
is true."
  (cond
    ((emptyp seq) nil)
    ((funcall pred (head seq)) (seq (head seq) (filter pred (tail seq))))
    (t (filter pred (tail seq)))))

(defun interleave (seq &rest seqs)
  "Produce a lazy sequence of the first item in each argument, then the
second, etc."
  (unless (emptyp seq)
    (seq (head seq)
         (apply #'interleave
                (car seqs)
                (append (cdr seqs) (list (tail seq)))))))

(defun zip (&rest seqs)
  "Produce a lazy sequence of tuples made up of the first element of
each argument, then second, etc."
  (apply #'mapcar* (lambda (&rest elts) (apply #'list elts)) seqs))

(defun enumerate (seq)
  "Produce a lazy sequence enumerating the elements of SEQ.
E.g. (0 elt0), (1 elt1), (2 elt2), etc."
  (zip (range) seq))

(defun constantly* (value)
  "Produce a lazy sequence for which the head is alway VALUE."
  (seq value (constantly* value)))

(defun repeatedly (f)
  "Given a function F, presumably with side effects, produce a lazy
sequence of repeated calls to F."
  (seq (funcall f) (repeatedly f)))

(defun iterate (f x)
  "Produce the lazy sequence of x, (f x), (f (f x)), (f (f (f x))), etc."
  (seq x (iterate f (funcall f x))))

(defun nth* (seq n &key not-found)
  "Return the Nth element of SEQ, or NOT-FOUND if no such element
exists."
g!  (or (head (drop n seq)) not-found))

;; Printing lazy lazy-sequences

(defun to-list (seq &key max)
  (labels ((aux (seq count accum)
             (cond
               ((or (and count (zerop count)) (null seq))
                (values (reverse accum) (not (emptyp seq))))
               (t (aux (tail seq) (and count (1- count)) (cons (head seq) accum))))))
    (aux seq max nil)))

(defparameter *sequence-max-print* 16)

(defmethod print-object ((object lazy-sequence) stream)
  (multiple-value-bind (list more) (to-list object :max *sequence-max-print*)
    (when more (setq list (append list (list '|...|))))
    (print-object list stream)))

